#include <vector>

class Histogram
{
  protected:
    std::string name;
    std::string metric;
    int bins, total_datapoints;
    double binwidth;
    bool finalized;
    std::vector<int> abs_histogram;
    std::vector<double> prob_histogram;
    std::vector<double> raw_data;

  public:
    void setName(std::string);
    void setMetric(std::string);
    Histogram(int bins);
    int getBins();
    bool check_status(bool expected);
    void addDataPoint(double datapoint);
    void addDataPoints(std::vector<double> data);
    void print_absolutes();
    void print_probabilities();
    std::string getProbRow();
    std::string getAbsRow();
    void writeToFile(std::string type, std::string filename);
    void finalize();
    void writeToCsv();
    void writeRaw();

};

