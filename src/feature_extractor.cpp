#include <iostream>
#include <fstream>
#include <random>
#include <math.h>
#include <algorithm>
#include <pcl/common/common.h>
#include <pcl/common/distances.h>
#include <pcl/io/pcd_io.h>
#include "boost/filesystem.hpp"
#include <boost/algorithm/string/replace.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;
#include "Histogram.h"
#include <opencv2/opencv.hpp>
#include "highgui.h"
#include <stdlib.h>

using namespace std;
//using namespace cv;

void create_histogram(string infile, int bincount, string feature);
void create_rgb_histogram(string infile, int bincount);
void create_ccol_histogram(string infile, int bincount);
double getDistance(pcl::PointXYZ p1, pcl::PointXYZ p2);
double getAngle(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3);
double getArea(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3);
float get_datapoint_count(cv::Mat mat);
void printPoint(pcl::PointXYZ p);
string get_csv_histogram(cv::Mat hist);
Eigen::Vector4f getVector(pcl::PointXYZ from, pcl::PointXYZ to);
bool areEqual(pcl::PointXYZ p1, pcl::PointXYZ p2);


string output_filename;
int main(int argc, char* argv[])
{

  int bins;
  string feature;
  po::options_description desc("Allowed options");
  desc.add_options()
      ("help", "produce help message")
      ("bins", po::value<int>(&bins)->default_value(10), "Set the number of bins for the feature")
      ("feature", po::value<string>(&feature)->default_value("da3"), "Specify the feature to extract (ccol, rgb64, da3, dd2, dd3)")
      ("output-file", po::value<string>(&output_filename)->default_value("kinectftrs.out"), "Specify the output filename")
      ("input-file", po::value< vector<string> >(), "Specify appropriate input files for chosen feature")
  ;

  po::positional_options_description p;
  p.add("input-file", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
      options(desc).positional(p).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
      cout << desc << "\n";
      return 1;
  }
  
  vector<string> input_files;
  if (vm.count("input-file")){
    input_files = vm["input-file"].as< vector<string> >();
  } 
  else {
    cerr << "No input files given!!" << endl;
  }

  for(std::vector<int>::size_type i = 0; i != input_files.size(); i++) {
      cout << input_files[i] << endl;
  }

  cout << "CONF:" << endl;
  cout << "BINS: " << bins << endl;
  cout << "FEATURE: " << feature << endl;
  cout << "OUTPUTFILE: " << output_filename << endl;

  for(std::vector<int>::size_type i = 0; i != input_files.size(); i++)
  {
    string filename = input_files[i];
    cout << "processing " << filename << endl; 
    if (feature == "rgb64")
    {
      create_rgb_histogram(filename, bins);
    }
    else if (feature == "ccol") 
    {
      create_ccol_histogram(filename, bins);
    }
    else if (feature == "dd2" || feature == "dd3" || feature == "da3")
    {
      create_histogram(filename, bins, feature);
    }
    else
    {
      cerr << "Feature not recognized" << endl;
      exit(1);
    }
  }
}

void create_ccol_histogram(string infile, int bincount)
{
  cv::Mat rgbframe = cv::imread(infile, 1);
  cv::Mat cframe(rgbframe.rows, rgbframe.cols, CV_16UC1);
  int rowlength = rgbframe.cols;
  for(int i=0; i<rgbframe.rows; i++)
  {
    for(int j=0; j<rgbframe.cols; j++)
    {
      cv::Vec3b pixel = rgbframe.at<cv::Vec3b>(i * rowlength + j);
      uchar b = pixel[0];
      uchar g = pixel[1];
      uchar r = pixel[2];
      uchar bs = b >> 5;
      uchar gs = g >> 5;
      uchar rs = r >> 5;
      ushort ccol = 0;
      ccol = (bs << 6) + (gs << 3) + rs;
      //if (ccol > 512 )
      //{
      //  cout << "this should not happen" <<endl;
      //  cout << ccol << endl;
      //  cout << bs << " " << gs << " " << rs << endl;
      //}
      cframe.at<ushort>(i,j) = ccol;
      //cout << ccol << "  <>  " << cframe.at<ushort>(i,j) << endl;
    }
  }
  //cout << cframe << endl;
  cv::Mat maskframe;
  string maskfile = infile; 
  boost::replace_all(maskfile, "crop", "maskcrop");

  boost::filesystem::path my_path( infile );
  string hname = my_path.filename().string();

  maskframe = cv::imread(maskfile, cv::IMREAD_GRAYSCALE);
  float range[] = {0, 512 };
  const float* histRange = { range };
  cv::Mat resHist;

  cv::calcHist( &cframe, 1, 0, maskframe, resHist, 1, &bincount, &histRange, true, false);
  //cout << resHist << endl;

  float datapoints = get_datapoint_count(resHist);

  resHist.convertTo( resHist, CV_64F, 1.0/datapoints );
  string hist_row = "ccol:prob:" + hname + ":" + get_csv_histogram(resHist);

  ofstream file;
  file.open(output_filename , std::fstream::app);
  file << hist_row << endl;
  file.close();
}

void create_rgb_histogram(string infile, int bincount)
{
  cv::Mat rgbframe;
  cv::Mat maskframe;
  string maskfile = infile; 
  boost::replace_all(maskfile, "crop", "maskcrop");
  cout << maskfile << endl;

  boost::filesystem::path my_path( infile );
  string hname = my_path.filename().string();

  rgbframe = cv::imread(infile, 1);
  maskframe = cv::imread(maskfile, cv::IMREAD_GRAYSCALE);

  vector<cv::Mat> bgr_planes;
  cv::split( rgbframe, bgr_planes);
  float range[] = { 0, 256 };
  const float* histRange = { range };
  bool uniform = true;
  bool accu = false;

  cv::Mat b_hist, g_hist, r_hist;
  cv::calcHist( &bgr_planes[0], 1, 0, maskframe, b_hist, 1, &bincount, &histRange, uniform, accu );
  cv::calcHist( &bgr_planes[1], 1, 0, maskframe, g_hist, 1, &bincount, &histRange, uniform, accu );
  cv::calcHist( &bgr_planes[2], 1, 0, maskframe, r_hist, 1, &bincount, &histRange, uniform, accu );

  float datapoints = get_datapoint_count(b_hist);

  b_hist.convertTo( b_hist, CV_64F, 1.0/datapoints );
  g_hist.convertTo( g_hist, CV_64F, 1.0/datapoints );
  r_hist.convertTo( r_hist, CV_64F, 1.0/datapoints );

  string blue_row = "blue:prob:" + hname + ":" + get_csv_histogram(b_hist);
  string green_row = "green:prob:" + hname + ":" + get_csv_histogram(g_hist);
  string red_row = "red:prob:" + hname + ":" + get_csv_histogram(r_hist);

  ofstream file;
  file.open(output_filename + "_blue", std::fstream::app);
  file << blue_row << endl;
  file.close();

  file.open(output_filename + "_green", std::fstream::app);
  file << green_row << endl;
  file.close();

  file.open(output_filename + "_red", std::fstream::app);
  file << red_row << endl;
  file.close();
}

float get_datapoint_count(cv::Mat mat)
{
  cv::MatIterator_<float> it, end;
  float datapoints = 0;
  for( it = mat.begin<float>(), end = mat.end<float>(); it != end; ++it)
  {
    datapoints += *it;
  }
  return datapoints;
}

string get_csv_histogram(cv::Mat hist)
{
  string result = "";
  cv::MatIterator_<double> it, end;
  for( it = hist.begin<double>(), end = hist.end<double>(); it != end; ++it)
  {
    result += boost::lexical_cast<std::string>(*it); 
    result += ",";
  }
  return result.substr(0, result.length() - 1);
}

void create_histogram(string infile, int bincount, string feature)
{
  string filename = infile;
  int distances = 100000;
  int bins = bincount;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename, *cloud) == -1)
  {
    cout << "ERROR: Something is wrong with the input file!" <<endl;
    return;
  }
  int total_points = cloud->points.size();
  if (total_points < 250)
  {
    return;
  }
  //Initialize Random Generator:
  random_device rd;
  mt19937 eng(rd());
  uniform_int_distribution<> distr(0, total_points - 1);

  //Data initialization
  boost::filesystem::path my_path( filename );
  string hname = my_path.stem().string();
  Histogram hist(bins);
  hist.setName(hname);
  hist.setMetric("dd3_64");
  for (int d = 0; d < distances; ++d)
  {
    //Get three points, making sure no duplicates occur:
    pcl::PointXYZ p1 = cloud->points[distr(eng)];
    pcl::PointXYZ p2 = cloud->points[distr(eng)];
    while(areEqual(p1, p2))
    {
      p2 = cloud->points[distr(eng)];
    }
    pcl::PointXYZ p3 = cloud->points[distr(eng)];
    while(areEqual(p1, p3) || areEqual(p2, p3) )
    {
      p3 = cloud->points[distr(eng)];
    }
    if (feature == "dd2")
    {
      double distance = getDistance(p1, p2);
      hist.addDataPoint(distance);
    }
    else if (feature == "dd3")
    {
      double angle = getAngle(p1, p2, p3);
      hist.addDataPoint(angle);
    }
    else if (feature == "da3")
    {
      double area = getArea(p1, p2, p3);
      hist.addDataPoint(area);
    }
  }
  hist.finalize();
  hist.writeToFile("prob", "part_" + output_filename);
}

double getDistance(pcl::PointXYZ p1, pcl::PointXYZ p2)
{
  return pcl::euclideanDistance(p1, p2);
}

void printPoint(pcl::PointXYZ p)
{
  cout << p.x << "," << p.y << "," << p.z << endl;
}


double getArea(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3)
{
  double sinus = sin( getAngle(p1, p2, p3));
  Eigen::Vector4f p1p2 = getVector(p1, p2);
  Eigen::Vector4f p1p3 = getVector(p1, p3);
  double area = 0.5 * p1p2.norm() * p1p3.norm() * sinus;
  return area;
}

Eigen::Vector4f getVector(pcl::PointXYZ from, pcl::PointXYZ to)
{
  Eigen::Map<Eigen::Vector4f, Eigen::Aligned> frommap = from.getVector4fMap();
  Eigen::Map<Eigen::Vector4f, Eigen::Aligned> tomap = to.getVector4fMap();
  Eigen::Vector4f result = Eigen::Vector4f(tomap - frommap);
  return result;
}

double getAngle(pcl::PointXYZ p1, pcl::PointXYZ p2, pcl::PointXYZ p3)
{
  Eigen::Map<Eigen::Vector4f, Eigen::Aligned> p1map = p1.getVector4fMap();
  Eigen::Map<Eigen::Vector4f, Eigen::Aligned> p2map = p2.getVector4fMap();
  Eigen::Map<Eigen::Vector4f, Eigen::Aligned> p3map = p3.getVector4fMap();
  Eigen::Vector4f p1p2 = Eigen::Vector4f(p2map - p1map);
  Eigen::Vector4f p1p3 = Eigen::Vector4f(p3map - p1map);
  double angle = pcl::getAngle3D(p1p2, p1p3);
  return angle;
}

bool areEqual(pcl::PointXYZ p1, pcl::PointXYZ p2)
{
  if(p1.x != p2.x) return false;
  if(p1.y != p2.y) return false;
  if(p1.z != p2.z) return false;
  return true;
}
