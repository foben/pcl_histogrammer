#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include "Histogram.h"
using namespace std;

Histogram::Histogram(int bins) : bins(bins), finalized(false)
{
  name = "Some Histogram";
  metric = "Some Metric";
  total_datapoints = 0;
  binwidth = 0;
  abs_histogram = std::vector<int>(bins, 0);
  prob_histogram = std::vector<double>(bins, 0.0);
  raw_data = std::vector<double>();
}

bool Histogram::check_status(bool expected)
{
  if (finalized == expected) return true;
  if (expected == true)  cerr << "Histogram is not yet finalized!" << endl;
  if (expected == false) cerr << "Histogram is already finalized!" << endl;
  return false;
}

int  Histogram::getBins() { return bins;}

void Histogram::setName(string newname)
{
  name = newname;
}

void Histogram::setMetric(string newmetric)
{
  metric = newmetric;
}

void Histogram::addDataPoint(double datapoint)
{
  if (!check_status(false)) return;
  total_datapoints += 1;
  raw_data.push_back(datapoint);
}

void Histogram::addDataPoints(vector<double> data)
{ 
  if (!check_status(false)) return;
  int points = data.size();
  for(int i = 0; i < points; ++i)
  {
    addDataPoint(data[i]);
  }
}

//Finalize the histogram:
//Get maximum value, calculate binwidth and populate absolute and prob. histograms
void Histogram::finalize()
{
  if (!check_status(false)) return;
  double max_val = *max_element(raw_data.begin(), raw_data.end());
  double contrib = 1.0 / total_datapoints;
  binwidth = max_val / bins;

  for (int i = 0; i < raw_data.size(); ++i)
  {
    double datapoint = raw_data[i];
    int bin = (int) (datapoint / binwidth);
    if (bin >= bins) bin = bins - 1;
    abs_histogram[bin] += 1;
    prob_histogram[bin] += contrib;
  }
  finalized = true;
}

void Histogram::print_absolutes()
{
  if (!check_status(true)) return;
  int total = 0;
  for (int i = 0; i < bins; ++i)
  {
    total += abs_histogram[i];
    cout << "Bin " << i << ": " << abs_histogram[i] << endl;
  }
  cout << "Total of " << total << " datapoints." << endl;
}

void Histogram::print_probabilities()
{
  if (!check_status(true)) return;
  double total = 0;
  for (int i = 0; i < bins; ++i)
  {
    total += prob_histogram[i];
    cout << "Bin " << i << ": " << prob_histogram[i] << endl;
  }
  cout << "Sums up to  " << total << endl;
}

string Histogram::getProbRow()
{
  if (!check_status(true)) return "";
  ostringstream row;
  row << metric << ":prob:" << name << ":";
  for (int i = 0; i < bins; ++i)
  {
    if (i != 0) row << ",";
    row << prob_histogram[i];
  }
  row << endl;
  return row.str();
}

string Histogram::getAbsRow()
{
  if (!check_status(true)) return "" ;
  ostringstream row;
  row << "abs:" << total_datapoints << ":" << name << ":";
  for (int i = 0; i < bins; ++i)
  {
    if (i != 0) row << ",";
    row << abs_histogram[i];
  }
  row << endl;
  return row.str();
}

void Histogram::writeRaw()
{
  if (!check_status(true)) return;
  ofstream toFile;
  toFile.open("rawdata.csv", std::fstream::app);
  toFile << name << ":";
  for (int i = 0; i < raw_data.size(); ++i)
  {
    if ( i != 0) toFile << ",";
    toFile << raw_data[i];
  }
  toFile << endl;
}

void Histogram::writeToFile(string type, string filename)
{
  if (!check_status(true)) return;
  string row;
  if (type == "abs") row = getAbsRow();
  else if (type == "prob") row = getProbRow();
  else
  {
    cerr << "Specify what to write, \"abs\" or \"prob\"!" << endl;
    return;
  }
  ofstream file;
  file.open(filename, std::fstream::app);
  file << row;
  file.close();
}

void Histogram::writeToCsv()
{
  if (!check_status(true)) return;
  ofstream toFile;
  toFile.open("test.csv", std::fstream::app);
  toFile << name << endl;
  toFile  << "Bin";
  for (int i = 0; i<bins; ++i)
  {
    toFile << "," << i*binwidth;
  }
  toFile << endl;
  toFile << "Prob";
  for (int i = 0; i<bins; ++i)
  {
    toFile << "," << prob_histogram[i];
  }
  toFile << endl;
  toFile << endl;

  toFile.close();
}

