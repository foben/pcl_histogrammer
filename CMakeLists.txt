CMAKE_MINIMUM_REQUIRED (VERSION 2.8)

SET (PROJ_NAME "Jibect Feature Extractor")
SET ( PROJ_SRC_DIR "src" )
SET ( PROJ_INC_DIR "inc" )
FILE ( GLOB_RECURSE PROJ_SOURCES src/*.cpp )
FILE ( GLOB_RECURSE PROJ_HEADERS inc/*.h )

PROJECT ( ${PROJ_NAME} )
FIND_PACKAGE(PCL 1.4 REQUIRED COMPONENTS common io visualization)
FIND_PACKAGE( Boost 1.40 COMPONENTS program_options)
FIND_PACKAGE( OpenCV REQUIRED )
INCLUDE_DIRECTORIES( ${PCL_INCLUDE_DIRS} )
INCLUDE_DIRECTORIES( ${PROJ_INC_DIR} )
INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIR} )
INCLUDE_DIRECTORIES( ${OpenCV_INCLUDE_DIRS} )
LINK_DIRECTORIES( ${PCL_LIBRARY_DIRS} )
ADD_DEFINITIONS( ${PCL_DEFINITIONS} )
ADD_EXECUTABLE ( feature_extractor ${PROJ_SOURCES} )
TARGET_LINK_LIBRARIES( feature_extractor  ${PCL_COMMON_LIBRARIES} ${PCL_IO_LIBRARIES} ${Boost_LIBRARIES} ${OpenCV_LIBS} )
SET(CMAKE_CXX_FLAGS "-std=c++0x")
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)
